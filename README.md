# Minetest Modding Book（中文版）

## 介绍
Minetest Modding Book（中文版）
由Chrithon本人和maxchen32翻译<br/>

**简体中文版的翻译尚未完成，英语四六级大佬，请帮助我们翻译或改进翻译；或者想提升英语阅读水平的用户，请移步至[英文原版](https://rubenwardy.com/minetest_modding_book/en/index.html)。**

## 如何参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
