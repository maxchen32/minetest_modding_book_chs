# 首页

**欢迎阅读Minetetst Modding Book（中文版）！**

> **请注意，该文档的翻译工作尚未完成，英语四六级大佬或者想提升英语阅读水平的同学请移步至[英文原版](https://rubenwardy.com/minetest_modding_book/en/index.html)。**<br/>
> **如果你想参与翻译工作的话，请使用电子邮箱（877259039@qq.com）联系译者本人，一旦文档翻译进度达到100%，那么你的用户名（或名字）会出现在译者名单上**<br/>
> **翻译进度：32%（开学后可能会咕咕咕）**

**如果你觉得这本书对你有帮助，可以考虑捐赠给原作者或者译者。**

**[让我们开始吧!](1.译者序.md)**

## 捐赠给Chrithon
请前往[Gitee主页](https://gitee.com/chrithon_official/minetest_modding_book_chs)

## 更多译者（欢迎加入）