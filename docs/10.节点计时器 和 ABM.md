---
title: 节点计时器 和 ABM
layout: default
root: ../..
idx: 3.2
description: 学习如何用ABM来改变方块
redirect_from:
- /zh/chapters/abms.html
- /zh/map/abms.html
---

## 介绍 <!-- omit in toc -->

在某些节点上运行周期性的函数是一个常见的任务。
Minetest为此提供了两种方法：Active Block Modifiers (ABMs) 和 node timers（节点计时器）。Active Block Modifier (缩写 ABM) 是在特定类型的节点上运行的函数，改变它们或使它们能够交互（例如草和苔藓生长）。

ABM会扫描所有加载的地图块(MapBlock)，查找符合条件的节点。
它非常适用于常见的节点，例如草。
CPU开销较高 ，但内存和存储开销较低。

对于不常见的或者已经有元数据的节点，如熔炉或某些机器，应该用节点计时器。
节点计时器通过跟踪每个地图块挂起的计时器来工作，然后到时间就运行它们。
这意味着计时器不需要搜索所有加载的节点来查找匹配项，只需要一点点资源来跟踪挂起的计时器。

- [节点计时器](#node-timers)
- [Active Block Modifiers](#active-block-modifiers)
- [轮到你了！](#your-turn)

## <span id="node-timers">节点计时器</span>

节点计时器直接绑定到单个节点。
可以通过 NodeTimerRef 对象来使用节点计时器。

```lua
local timer = minetest.get_node_timer(pos)
timer:start(10.5) -- 单位是秒
```

节点计时器一到时间，就会调用节点定义表中的 `on_timer` 方法。这个方法只接收一个参数——节点的位置：

```lua
minetest.register_node("autodoors:door_open", {
    on_timer = function(pos)
        minetest.set_node(pos, { name = "autodoors:door" })
        return false
    end
})
```

在 `on_timer` 中返回 true 会使计时器再次等待相同的间隔后，再运行。
也可以在 `on_timer` 中调用 `get_node_timer(pos)`，只需确保返回 false 以避免冲突。

你可能已经注意到了计时器的限制：出于优化原因，每种节点类型只能有一种类型的计时器，并且每个节点只能运行一个计时器。


## Active Block Modifiers

在本节中，外星草(alien grass)是一种草，有机会出现在水边。


```lua
minetest.register_node("aliens:grass", {
    description = "Alien Grass",
    light_source = 3, -- 节点发光：最小 0，最大 14
    tiles = {"aliens_grass.png"},
    groups = {choppy=1},
    on_use = minetest.item_eat(20)
})

minetest.register_abm({
    nodenames = {"default:dirt_with_grass"},
    neighbors = {"default:water_source", "default:water_flowing"},
    interval = 10.0, -- 每 10 秒运行一次
    chance = 50, -- 选择 50 个节点中的 1 个，概率是1/50
    action = function(pos, node, active_object_count,
            active_object_count_wider)
        local pos = {x = pos.x, y = pos.y + 1, z = pos.z}
        minetest.set_node(pos, {name = "aliens:grass"})
    end
})
```

这个 ABM 每10秒运行一次，对于每个匹配的节点，有50 分之一的概率运行action函数。
本例中如果 ABM 在某个节点上运行，则会在其上方放置一个外星草节点。
请注意，这将删除以前在该位置的节点。
为了防止这种情况，我们应该使用 `minetest.get_node` 进行检查，确保有空间生草。;-)

neighbors 是可选的。
如果 neighbors 中指定了多种物体，则只要挨着其中一种就符合要求。

chance 也是可选的。
如果不指定 chance ，ABM 会始终在其他条件满足时运行。

## 轮到你了！

* 点金术：每 5 秒以 100 分之一的概率使水变成金块。
* 腐烂：当水为邻时，使木头变成泥土。
* 燃烧！(Burnin'): 让每个空气节点着火。（提示：“air”和“fire:basic_flame”）。
  警告：游戏大概会崩溃。
